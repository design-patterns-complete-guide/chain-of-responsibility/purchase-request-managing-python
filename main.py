# Define an abstract class for the Approver in the chain.
class Approver:
    def __init__(self):
        self.next_approver = None  # Reference to the next Approver in the chain
        self.approval_limit = 0   # The maximum amount this Approver can approve

    def set_next_approver(self, next_approver):
        self.next_approver = next_approver  # Set the next Approver in the chain

    def process_request(self, amount):
        if amount <= self.approval_limit:
            self.approve_request()  # If the amount is within the limit, approve the request
        elif self.next_approver is not None:
            self.next_approver.process_request(amount)  # Pass the request to the next Approver
        else:
            print("Request denied by all approvers.")  # If no Approver can approve, deny the request

    def approve_request(self):
        pass  # Abstract method to be implemented by concrete Approvers

# Concrete Approver class for a Manager
class Manager(Approver):
    def __init__(self):
        super().__init__()
        self.approval_limit = 1000  # Manager's approval limit

    def approve_request(self):
        print("Manager has approved the request.")  # Approval action for Manager

# Concrete Approver class for a Director
class Director(Approver):
    def __init__(self):
        super().__init__()
        self.approval_limit = 5000  # Director's approval limit

    def approve_request(self):
        print("Director has approved the request.")  # Approval action for Director

# Concrete Approver class for a CEO
class CEO(Approver):
    def __init__(self):
        super().__init__()
        self.approval_limit = 10000  # CEO's approval limit

    def approve_request(self):
        print("CEO has approved the request.")  # Approval action for CEO

# Client code
manager = Manager()
director = Director()
ceo = CEO()

manager.set_next_approver(director)  # Set up the chain of responsibility
director.set_next_approver(ceo)

purchase_amount = 6000
manager.process_request(purchase_amount)  # Process a purchase request